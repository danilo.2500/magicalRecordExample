//
//  PessoaStore.swift
//  testeMagicalRecord
//
//  Created by Danilo Henrique on 28/03/18.
//  Copyright © 2018 Danilo Henrique. All rights reserved.
//

import Foundation
import MagicalRecord

class PessoaStore{

    private static let defaultContext = NSManagedObjectContext.mr_default()

    //evitando que essa classe seja instanciada
    private  init(){}
    
    //criando e salvando uma pessoa
    static func criarPessoa( nome: String, sobrenome: String ){
        
        let pessoa = Pessoa.mr_createEntity()!
 
        pessoa.nome = nome
        pessoa.sobrenome = sobrenome
        
        DatabaseManager.salvarAlteracoesNoBanco()
        
    }
    
    static func buscarTodosNoBanco() -> [Pessoa]?{
        
        return Pessoa.mr_findAll() as? [Pessoa]
        
    }
    
    static func buscarPessoaQue( _ atributo: String, temValor valor: String) -> Pessoa?{
        //Retorna o primero resultado e ignora os restantes
        //EX: se vc buscar por alguem com nome danilo e tiver 2 no banco, apenas 1 irá ser retornado pela funçao
        return Pessoa.mr_findFirst(byAttribute: atributo, withValue: valor)
    }
    
    static func adicionarMoradiaAo(_ morador: Pessoa, naCasa casa: Casa){
        
        casa.addToMoradores(morador)
        
        DatabaseManager.salvarAlteracoesNoBanco()
        
    }
    
    
    
//    static func deletarTodasPessoas(){
//        Pessoa.mr_truncateAll()
//
//    }
//
//    func deletar(pessoa: Pessoa){
//        pessoa.mr_deleteEntity()
//    }

}
