//
//  CasaStore.swift
//  testeMagicalRecord
//
//  Created by Danilo Henrique on 28/03/18.
//  Copyright © 2018 Danilo Henrique. All rights reserved.
//

import Foundation
import MagicalRecord

class CasaStore {
    
    private static let defaultContext = NSManagedObjectContext.mr_default()
    
    //evitando que essa classe seja instanciada
    private init(){}
    
    //criando e salvando uma Casa
    
    static func criarCasa( endereco: String ){
        
        let casa = Casa.mr_createEntity()!
        
        casa.endereco = endereco
        
        DatabaseManager.salvarAlteracoesNoBanco()
    }
    
    //Todas as alteraçoes que foram feitas nos objetos serão salvas no banco
//    static func salvarAlteraçõesNoBanco(){
//        defaultContext.mr_saveToPersistentStoreAndWait()
//    }
    
    static func buscarTodosNoBanco() -> [Casa]?{
        
        return Casa.mr_findAll() as? [Casa]
        
    }
    
    static func buscarCasaQue( _ atributo: String, temValor valor: String) -> Casa?{
        //Retorna o primero resultado e ignora os restantes
        //EX: se vc buscar por alguem com nome danilo e tiver 2 no banco, apenas 1 irá ser retornado pela funçao
        return Casa.mr_findFirst(byAttribute: atributo, withValue: valor)
    }
    
    static func adicionarMoradores(_ moradores: Pessoa, naCasa casa: Casa){
        
        adicionarMoradores( [moradores], naCasa: casa)
        
    }
    static func adicionarMoradores(_ moradores: [Pessoa], naCasa casa: Casa){
        
        casa.addToMoradores( NSSet(array: moradores) )
       
        DatabaseManager.salvarAlteracoesNoBanco()
    }
//    
//    static func deletarTodasCasas(){
//        Casa.mr_truncateAll()
//    }
//    static func deletar(casa: Casa){
//        casa.mr_deleteEntity()
//    }
    
}
