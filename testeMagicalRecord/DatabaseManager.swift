//
//  Banco.swift
//  testeMagicalRecord
//
//  Created by Danilo Henrique on 28/03/18.
//  Copyright © 2018 Danilo Henrique. All rights reserved.
//

import Foundation
import MagicalRecord

class DatabaseManager{
    static func deletarTodasInstanciasNoBancoDesta(entidade: NSManagedObject.Type){
        //se passar uma pessoa, deleta todas as pessoas
        entidade.mr_truncateAll()
        salvarAlteracoesNoBanco()
    }
    
    static func deletarNoBanco(_ instancia: NSManagedObject){
        instancia.mr_deleteEntity()
        salvarAlteracoesNoBanco()
    }
    
    static func salvarAlteracoesNoBanco(){
        NSManagedObjectContext.mr_default().mr_saveToPersistentStoreAndWait()
    }
    
   
}
